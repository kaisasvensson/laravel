<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\OrderItem
 *
 * @property int $id
 * @property int|null $order_id
 * @property int|null $item_id
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $name
 * @property string|null $sku
 * @property float|null $qty
 * @property float|null $price
 * @property float|null $tax_amount
 * @property float|null $row_total
 * @property float|null $price_incl_tax
 * @property float|null $total_incl_tax
 * @property float|null $tax_percent
 * @property int|null $amount_package
 * @property-read \App\Order|null $order
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderItem whereAmountPackage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderItem whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderItem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderItem whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderItem wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderItem wherePriceInclTax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderItem whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderItem whereRowTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderItem whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderItem whereTaxAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderItem whereTaxPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderItem whereTotalInclTax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OrderItem extends Model
{

  protected $primaryKey = 'id';
  public $incrementing = false;
  public $timestamps = false;

  protected $fillable = [
    "id",
    "order_id",
    "item_id",
    "created_at",
    "updated_at",
    "name",
    "sku",
    "qty",
    "price",
    "tax_amount",
    "row_total",
    "price_incl_tax",
    "total_incl_tax",
    "tax_percent",
    "amount_package" ];

  public function order() {
    return $this->belongsTo(Order::class);
  }
}

