<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\GroupPrice
 *
 * @property float $price
 * @property int $group_id
 * @property int $product_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Customer[] $customer
 * @property-read \App\Group $group
 * @property-read \App\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GroupPrice whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GroupPrice wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GroupPrice whereProductId($value)
 * @mixin \Eloquent
 */
class GroupPrice extends Model
{
  public $incrementing = false;
  public $timestamps = false;

  protected $fillable = [
    "price",
    "group_id",
    "product_id" ];

  public function customer() {
    return $this->belongsToMany(Customer::class);
  }
  public function group() {
    return $this->belongsTo(Group::class);
  }
  public function product() {
    return $this->belongsTo(Product::class);
  }
}

