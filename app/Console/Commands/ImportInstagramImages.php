<?php

namespace App\Console\Commands;

use App\InstagramImage;
use Illuminate\Console\Command;

class ImportInstagramImages extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'import:instagram';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'import instagram images';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle() {
    $curl = curl_init();

    curl_setopt_array($curl, [
      CURLOPT_URL => "https://api.instagram.com/v1/users/self/media/recent?access_token=173643874.47a75c5.c88ecb7a31744e1884bc59dc84059e09",
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => [
        "cache-control: no-cache",
        "postman-token: dc46c43a-ede9-9a3c-0f56-2aaeff2dc2f6"
      ],
    ]);

    $response = json_decode(curl_exec($curl), TRUE);
    $err = curl_error($curl);
    curl_close($curl);

    foreach ($response['data'] as $image) {
      $this->info("Inserting/updating image with id: " . $image['id']);
      $dbImage = InstagramImage::findOrNew($image['id']);
      $url = $image['images']['standard_resolution']['url'];
      $dbImage->fill([
        'id' => $image['id'],
        'url' => $url,
      ])->save();

    }
  }
}
