<?php

namespace App\Console\Commands;

use App\BillingAddress;
use App\Order;
use App\OrderItem;
use App\ShippingAddress;
use Illuminate\Console\Command;

class ImportOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importera odrar';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
  public function handle()
  {

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://www.milletech.se/invoicing/export/",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "postman-token: 60f103b1-739e-66cf-d703-99cba96af44a"
      ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);
    $err = curl_error($curl);

    curl_close($curl);

    foreach ($response as $order) {
      $this->info("Inserting/updating order with id: " . $order['id']);
      if ($order['status'] != 'processing') continue;
      $dbCustomer = Order::findOrNew($order['id']);
      $dbCustomer->fill($order)->save();


      if (isset($order['shipping_address']) && is_array($order['shipping_address'])) {
        $dbShipping = ShippingAddress::findOrNew($order['shipping_address']['id']);
        $dbShipping->fill($order['shipping_address'])->save();
      }

      if (isset($order['billing_address']) && is_array($order['billing_address'])) {
        $dbBilling = BillingAddress::findOrNew($order['billing_address']['id']);
        $dbBilling->fill($order['billing_address'])->save();
      }
      foreach ($order['items'] as $item){
        $dbItem = OrderItem::findOrNew($item['id']);
        $dbItem->fill($item)->save();
      }

    }

  }
}