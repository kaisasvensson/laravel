<?php

namespace App\Console\Commands;

use App\TwitterTweet;
use Illuminate\Console\Command;

class ImportTwitterTweets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:tweets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tweets with the word cat';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.twitter.com/1.1/search/tweets.json?q=christmascat",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "authorization: Bearer AAAAAAAAAAAAAAAAAAAAANar3QAAAAAArPBMtnXA%2Fw6xWbSHU%2FEJrJp951Q%3DweoAzq1D57QNeiLXwVPb5iGB8Kjcll12V7cFwiN1PUEEJU6mec",
          "cache-control: no-cache",
          "postman-token: 0bf48275-c7a7-0db3-8d40-cf0016816f6e"
        ),
      ));

      $response = json_decode(curl_exec($curl), TRUE);
      $err = curl_error($curl);
      curl_close($curl);

      foreach ($response['statuses'] as $tweet) {
        $this->info("Inserting tweet with id: " . $tweet['id']);
        $dbTweet = TwitterTweet::findOrNew($tweet['id']);
        $text = $tweet['text'];
        $dbTweet->fill($tweet)->save();
      }

    }
}
