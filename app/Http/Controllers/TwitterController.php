<?php

namespace App\Http\Controllers;

use App\TwitterTweet;
use Illuminate\Http\Request;

class TwitterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

/*      return response()->json(Tw::all());*/
    }

    public function countWords(){
      $tweets = TwitterTweet::all('text');
      $result = TwitterTweet::count_words($tweets);
      return response()->json($result);
  }

    function excludeWords(){
      $tweets = TwitterTweet::all('text');
      $result = TwitterTweet::excludeWords($tweets);
      return response()->json($result);
  }

  public function searchTweet(){

    $tweet = [];
    $searchWord = "";
    if(isset($_GET['search'])) {

      $searchWord = $_GET['search'];
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.twitter.com/1.1/search/tweets.json?q='.$searchWord.'",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "authorization: Bearer AAAAAAAAAAAAAAAAAAAAANar3QAAAAAArPBMtnXA%2Fw6xWbSHU%2FEJrJp951Q%3DweoAzq1D57QNeiLXwVPb5iGB8Kjcll12V7cFwiN1PUEEJU6mec",
          "cache-control: no-cache",
          "postman-token: 5ee9fa45-db36-7ece-6161-0f24a1b4352a"
        ),
      ));

      $response = json_decode(curl_exec($curl), true);
      $err = curl_error($curl);
      curl_close($curl);

      $tweet = TwitterTweet::findTweet($response);
    }
    return view('twittersearch', ['tweets' => $tweet]);
  }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
