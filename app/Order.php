<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Order
 *
 * @property string|null $increment_id
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $customer_id
 * @property string|null $customer_email
 * @property string|null $status
 * @property string|null $marking
 * @property float|null $grand_total
 * @property float|null $subtotal
 * @property float|null $tax_amount
 * @property int|null $billing_address_id
 * @property int|null $shipping_address_id
 * @property string|null $shipping_method
 * @property float|null $shipping_amount
 * @property float|null $shipping_tax_amount
 * @property string|null $shipping_description
 * @property int $id
 * @property-read \App\BillingAddress $billing_address
 * @property-read \App\Customer|null $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\OrderItem[] $order_item
 * @property-read \App\ShippingAddress $shipping_address
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereBillingAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereCustomerEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereGrandTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereIncrementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereMarking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereShippingAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereShippingAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereShippingDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereShippingMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereShippingTaxAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereSubtotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereTaxAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Order extends Model
{

  protected $primaryKey = 'id';
  public $incrementing = false;
  public $timestamps = false;

  protected $fillable = [
    "increment_id",
    "created_at",
    "updated_at",
    "customer_id",
    "customer_email",
    "status",
    "marking",
    "grand_total",
    "subtotal",
    "tax_amount",
    "billing_address_id",
    "shipping_address_id",
    "shipping_method",
    "shipping_amount",
    "shipping_tax_amount",
    "shipping_description",
    "id" ];

  public function customer() {
    return $this->belongsTo(Customer::class);
  }
  public function orderItem() {
    return $this->hasMany(OrderItem::class);
  }
  public function shippingAddress() {
    return $this->hasOne(ShippingAddress::class);
  }
  public function billingAddress() {
    return $this->hasOne(BillingAddress::class);
  }


}
