<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Role
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @mixin \Eloquent
 */
class Role extends Model
{
    public function users() {
      return $this->belongsToMany(User::class);
    }
}

//laravel tror nu att det finns en tabell som heter roles