<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstagramImage extends Model
{
  public $incrementing = false;
  public $timestamps = false;

  protected $fillable = [
    "id",
    "url" ];
}
