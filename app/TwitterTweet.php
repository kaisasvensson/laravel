<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TwitterTweet extends Model
{
  public $incrementing = false;
  public $timestamps = false;

  protected $fillable = [
    "id",
    "text" ];

  public static function count_words($tweets) {
    $words = [];
    foreach ($tweets as $tweet) {
      $words = array_merge($words, explode(" ", $tweet));
    }
    return (array_count_values($words));
  }

  public static function excludeWords($tweets) {

    $stopWords = [
      'if',
      'so',
      'be',
      'to'];

   $words = [];
    foreach ($tweets as $tweet) {
      $words = array_merge($words, explode(" ", $tweet));
    }

    foreach ($words as $key => $value) {
      if (in_array($value,$stopWords)){
      unset($words[$key]);
    }
  }
    return (array_count_values($words));
  }


  public static function findTweet($response)
  {
    $words = [];
    foreach ($response['statuses'] as $tweet) {
      $words = array_merge($words, explode(" ", $tweet['text']));
    }
    $sort = array_count_values($words);
    asort($sort);
    return array_reverse($sort);
  }

}

