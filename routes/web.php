<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
  return view('welcome');
});
Route::resource('customers', 'CustomerController');

/*
Route::get('/customers', 'CustomersController@showCustomers');

Route::get('/customers/{id}', 'CustomersController@showCustomerId');

Route::get('/customers/{id}/address', 'CustomersController@showCustomerAddress');*/


Route::get('/companies', 'CustomersController@showCompanies');

Route::get('/companies/by-company/{id}', 'CustomersController@showCompanyId');


Route::get('/fb-login', 'FacebookController@index');

Route::get('/login', 'FacebookController@loginForm');

Route::get('/facebook', 'FacebookController@facebook');


Route::resource('product','ProductController');


Route::get('/stripe', 'StripeController@index');


Route::get('/images', 'InstagramImageController@index');


Route::get('/klarna', 'KlarnaController@index');

Route::get('/klarna-confirmation', 'KlarnaController@confirmation');

Route::get('/klarna-acknowledge', 'KlarnaController@acknowledge');

Route::get('/twitter', 'TwitterController@countWords');

Route::get('/stopwords', 'TwitterController@excludeWords');

Route::get('/twittersearch', 'TwitterController@searchTweet');
