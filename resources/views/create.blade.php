<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

</head>
<body>

<h2>PRODUCT</h2>

<form action="<?= action('ProductController@store') ?>" method="post">

  <?= csrf_field() ?>

    <input type="text" name="entity_id" placeholder="ID">

    <input type="text" name="sku" placeholder="SKU">

    <input type="text" name="name" placeholder="Name">

    <input type="text" name="amount_package" placeholder="Amount/Package">

    <input type="text" name="price" placeholder="Price">

    <input type="submit" name="submit" value="Submit">
</form>



</body>
</html>
